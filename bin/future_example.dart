Future<String> createrOrderMessage() async {
  var order = await fetchUserorder();
  return 'Your order is: $order';
}

Future<String> fetchUserorder() => Future.delayed(
      const Duration(seconds: 2),
      () => 'Large Latte',
    );

void main() async {
  print('Fetching user order...');
  print(await createrOrderMessage());
  print('End of create order message');
}
